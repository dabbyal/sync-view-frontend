import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
};

@Injectable()
export class AuthService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:1234/sync-view-backend/auth';
  }

  public getById(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + '');
  }

  public save(user: User) {
    return this.http.post<User>(this.usersUrl + '/register', user);
  }
}
